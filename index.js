
// Use "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" let Node.js transfer data using the Hyper Text Transfer Protocol
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (devices/browser) and server (nodeJS/expressJS application) communicate by exchanging individual messages (request/responses)
// Request - the message sent by the client 
// Response - the message sent by the server as response

let http = require("http");

// The "http module" has createServer() method that accepts a function as an argument and allows for a creation of a server using Node.js

							// req, res
// no need to invoke		// request, response
http.createServer(function(request, response) { 
	// Use writeHead() method to:
		// Set a status code for the response. (200 means okay or success)
			// HTTP Response status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

		// Set the content-type of the response

	response.writeHead(200, {"Content-Type" : "text/plain"});

	response.end("Hello World");

}).listen(3000); // 3000 - A port is a virtual point where network connections start and end
// Each port(number) is associated with specific process or services
// The server will be assigned to port 3000 via the listen() method
	// Where the server will listen to any request that are sent to it and will send the response via this port
// Port numbers: 3000, 4000, 5000, 8000 - usually used for web development

console.log("Server is running at localhost:3000");
console.log("You may now use the application");
console.log("Welcome to my application");
// We will access the server in the browser using the localhost:4000
// All console.log will run in the terminal


// Commands to run our web application in Terminal
/*
	1: Install nodemon package
		- npm install -g nodemon / sudo npm install -g nodemon
			or npm i -g nodemon
	
	2: Once nodemon packages are already installed/downloaded, you may not install the nodemon again
		- nodemon (file to access)
*/
































